// smelly code wrote by Rima Zourane
package org.refactor_IntelliJ;

class Tea implements DrinkI, CoffeeI {
    private static final double BASIC_PRICE = 2.0;
    private static final double EXTRA_FEE = 1.0;

    public double calculatePrice() {
        return BASIC_PRICE + EXTRA_FEE;
    }
}

