// smelly code wrote by Rima Zourane
package org.refactor_IntelliJ;

class Coffee implements CoffeeI, DrinkI {
    private static final double BASIC_PRICE = 2.0;
    private static final double EXTRA_FEE = 1.5;
    @Override
    public double calculatePrice() {
        return BASIC_PRICE + EXTRA_FEE;
    }
}