// smelly code wrote by Rima Zourane
package org.refactor_IntelliJ;
// Main class with smelly code
public class Main {
    public static void main(String[] args) {
        // Example command: Coffee, Tea, Juice
        CoffeeI coffee = new Coffee();
        DrinkI herbata = new Tea();
        Tea tea = new Tea();
        Juice juice = new Juice();

        // Calculate the total price for the command
        double total = calculateTotal(coffee, tea, juice, herbata);

        // Display the total price
        System.out.println("Total Price: $" + total);
    }

    // Smelly method with common method names for different classes
    private static double calculateTotal(Object... drinks) {
        double total = 0.0;
        for (Object drink : drinks) {
            if (drink instanceof Coffee) {
                total += ((Coffee) drink).calculatePrice();
            } else if (drink instanceof Tea) {
                total += ((Tea) drink).calculatePrice();
            } else if (drink instanceof Juice) {
                total += ((Juice) drink).calculatePrice();
            }
        }
        return total;
    }
}