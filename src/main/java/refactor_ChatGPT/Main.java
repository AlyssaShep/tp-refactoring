// smelly code wrote by Rima Zourane
package refactor_ChatGPT;

import java.util.Arrays;
import java.util.List;

// Main class with smelly code
public class Main {
    public static void main(String[] args) {
        // Exemple de commande : Coffee, Tea, Juice
        List<Drink> drinks = Arrays.asList(new Coffee(), new Tea(), new Juice());

        // Calculer le prix total
        double total = calculateTotal(drinks);

        // Afficher le prix total
        System.out.println("Total Price: $" + total);
    }

    private static double calculateTotal(List<Drink> drinks) {
        return drinks.stream().mapToDouble(Drink::calculatePrice).sum();
    }
}