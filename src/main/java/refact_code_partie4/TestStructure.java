package refact_code_partie4;

public class TestStructure {

    public static void main(String[] args) {
        // Test de ListeTableau
        System.out.println("=== Test de ListeTableau ===");
        Structure listeTableau = new ListeTableau();
        testMethods(listeTableau);

        // Test de ListeChainee
        System.out.println("\n=== Test de ListeChainee ===");
        Structure listeChainee = new ListeChainee();
        testMethods(listeChainee);

        // Test de QueueDoubleEntree
        System.out.println("\n=== Test de QueueDoubleEntree ===");
        Structure queueDoubleEntree = new QueueDoubleEntree();
        testMethods(queueDoubleEntree);

        // Test de QueueAvecPriorite
        System.out.println("\n=== Test de QueueAvecPriorite ===");
        Structure queueAvecPriorite = new QueueAvecPriorite();
        testMethods(queueAvecPriorite);
    }

    /**
     * Méthode pour tester les méthodes de l'interface Structure.
     *
     * @param structure l'objet implémentant l'interface Structure.
     */
    private static void testMethods(Structure structure) {
        // Test de la méthode add
        System.out.println("Résultat de la méthode add: " + structure.add("Test"));

        // Test de la méthode isEmpty
        System.out.println("La structure est vide? " + structure.isEmpty());

        // Test de la méthode get (avec un index arbitraire pour l'exemple)
        System.out.println("Élément à l'index 0: " + structure.get(0));

        // Test de la méthode peek
        System.out.println("Élément en haut de la structure: " + structure.peek());

        // Test de la méthode poll
        System.out.println("Élément retiré de la structure: " + structure.poll());
    }
}
