package refact_code_partie4;

public interface Structure {

    /**
     * Ajoute un objet à la structure.
     *
     * @param o l'objet à ajouter.
     * @return true si l'ajout a réussi, sinon false.
     */
    boolean add(Object o);

    /**
     * Vérifie si la structure est vide.
     *
     * @return true si la structure est vide, sinon false.
     */
    boolean isEmpty();

    /**
     * Récupère un objet à partir de la structure basé sur son index ou sa position.
     *
     * @param i l'index ou la position de l'objet à récupérer.
     * @return l'objet récupéré ou null s'il n'existe pas.
     */
    Object get(int i);

    /**
     * Récupère l'objet en haut de la structure sans le retirer.
     *
     * @return l'objet en haut de la structure ou null s'il n'y en a pas.
     */
    Object peek();

    /**
     * Récupère et retire l'objet en haut de la structure.
     *
     * @return l'objet retiré ou null s'il n'y en a pas.
     */
    Object poll();
}
