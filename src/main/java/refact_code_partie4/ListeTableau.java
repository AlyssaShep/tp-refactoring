package refact_code_partie4;

public class ListeTableau implements Structure {
    public boolean add(Object o) {return true;}
    public boolean isEmpty() {return true;}
    public Object get(int i) {return null;}

    @Override
    public Object peek() {
        return null;
    }

    @Override
    public Object poll() {
        return null;
    }

    private void secretLT(){}
    public static void staticLT() {}
    int nbLT;
}
class ListeChainee implements Structure {
    public boolean add(Object o) {return true;}
    public boolean isEmpty() {return true;}
    public Object get(int i) {return null;}
    public Object peek() {return null;}
    public Object poll() {return null;}
    private void secretLC(){}
}
class QueueDoubleEntree implements Structure {
    public boolean add(Object o) {return true;}
    public boolean isEmpty() {return true;}

    @Override
    public Object get(int i) {
        return null;
    }

    public Object peek() {return null;}
    public Object poll() {return null;}
    private void secretQDE(){}
}
class QueueAvecPriorite implements Structure {
    public boolean add(Object o) {return true;}
    public boolean isEmpty() {return true;}

    @Override
    public Object get(int i) {
        return null;
    }

    public Object peek() {return null;}
    public Object poll() {return null;}
    public Object comparator() {return null;}
    private void secretQAP(){}
}